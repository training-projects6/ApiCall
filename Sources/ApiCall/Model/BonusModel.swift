//
//  BonusModel.swift
//  iProBonus
//
//  Created by Artem Soloviev on 23.06.2023.
//

import Foundation


public struct Welcome: Codable {
    public var resultOperation: ResultOperation
    public var data: DataClass
    public init(resultOperation: ResultOperation, data: DataClass) {
        self.resultOperation = resultOperation
        self.data = data
    }
}

public struct DataClass: Codable {
    public var typeBonusName: String
    public var currentQuantity: Int
    public var forBurningQuantity: Int
    public var dateBurning: String
    
    public init(typeBonusName: String, currentQuantity: Int, forBurningQuantity: Int, dateBurning: String) {
        self.typeBonusName = typeBonusName
        self.currentQuantity = currentQuantity
        self.forBurningQuantity = forBurningQuantity
        self.dateBurning = dateBurning
    }
}

public struct ResultOperation: Codable {
    public var status: Int
    public var message: String
    public var messageDev: String?
    public var codeResult: Int
    public var duration: Int
    public var idLog: String
    
    public  init(status: Int, message: String, messageDev: String? = nil, codeResult: Int, duration: Int, idLog: String) {
        self.status = status
        self.message = message
        self.messageDev = messageDev
        self.codeResult = codeResult
        self.duration = duration
        self.idLog = idLog
    }
}
