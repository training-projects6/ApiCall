//
//  PostCallModel.swift
//  iProBonus
//
//  Created by Artem Soloviev on 23.06.2023.
//

import Foundation


struct PostCall: Codable {
    
    var idClient = "2c44d8c2-c89a-472e-aab3-9a8a29142315"
    var paramValue = "7db72635-fd0a-46b9-813b-1627e3aa02ea"
}

struct WelcomePostCall: Codable {
    var result: ResultPostCall
    var accessToken: String
}

struct ResultPostCall: Codable {
    var status: Int
    var message: String
    var messageDev: String?
    var codeResult: Int
    var duration: Int
    var idLog: String
}
